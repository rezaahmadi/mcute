/**
 * @author reza
 */
// Copyright (c) 2018, Reza Ahmadi (ahmadi@cs.queensu.ca)


#ifndef __BRANCH__UTIL__H
#define __BRANCH__UTIL__H

#include <assert.h>
#include <stdio.h>
//#include <sys/time.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>


#include <limits>
#include <cstdlib>

#include <ctime>

#include <concolic_search.h>
#include <fileutil.h>
#include <coverage_util.h>
#include "base/basic_types.h"
#include "base/symbolic_execution.h"
#include "base/symbolic_expression.h"
#include "base/symbolic_interpreter.h"
#include "base/symbolic_path.h"
#include "base/symbolic_predicate.h"
#include "base/yices_solver.h"



using namespace std;

namespace mcute {

	//typedef long long int value_t;

	/*
	* author: reza
	* */

	class branch_util {


	public:
		static void negate_rand(const SymbolicExecution& ex, int& depth);
		static void negate_sys(const SymbolicExecution& ex, int& depth, map<string, coverage_util*> coverage_util_table, string next_t);

	private:
		static bool solveAtBranch(const SymbolicExecution& ex, int branch_idx, vector<value_t>& input);
	};

}

#endif
